import Todo from '../../../../src/model/todo'
import chai from 'chai'
const expect = chai.expect

describe('Class todo', () => {
  it('can be created with arguments', () => {
    const id = Math.random() * 1000
    const title = 'Test title'
    const done = true
    const description = 'Test Description'
    const deadline = '2016/06/06'
    const notifyTime = '2016/07/07'
    const priority = 3
    const tags = ['UI', 'BUG']
    let todo = Todo.with({
      id: id,
      title: title,
      done: done,
      description: description,
      deadline: deadline,
      notifyTime: notifyTime,
      priority: priority,
      tags: tags
    })
    expect(todo.id).to.equal(id)
    expect(todo.title).to.equal(title)
    expect(todo.done).to.equal(done)
    expect(todo.description).to.equal(description)
    expect(todo.deadline).to.equal(deadline)
    expect(todo.notifyTime).to.equal(notifyTime)
    expect(todo.priority).to.equal(priority)
    expect(todo.tags).to.equal(tags)
  })
})
