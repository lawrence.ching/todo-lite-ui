import { mutations } from '../../../../src/store/todos'
import { expect } from 'chai'

const { setTodos } = mutations

describe('Store todos mutations', () => {
  it('setTodos', () => {
    const state = { todos: [] };
    const todos = [{
      id: Math.random() * 10
    }]
    setTodos(state, todos)
    expect(state.todos.length).to.equal(1)
    expect(state.todos[0].id).to.equal(todos[0].id)
  })
})
