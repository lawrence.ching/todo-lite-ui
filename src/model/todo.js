class Todo {

  constructor () {
    this.id = null
    this.title = ''
    this.done = false
    this.description = ''
    this.deadline = null
    this.notifyTime = null
    this.priority = 0
    this.tags = []
  }

  static with (argus) {
    let todo = new Todo()
    if (argus) {
      todo.id = argus.id || todo.id
      todo.title = argus.title || todo.title
      todo.done = argus.done || todo.done
      todo.description = argus.description || todo.description
      todo.deadline = argus.deadline || todo.deadline
      todo.notifyTime = argus.notifyTime || todo.notifyTime
      todo.priority = argus.priority || todo.priority
      todo.tags = argus.tags || todo.tags
    }
    return todo
  }
}

export default Todo
