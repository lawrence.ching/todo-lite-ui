import Api from '../api'

const state = {
  todos: []
}

const getters = {
  getDoneTodos: state => {
    return state.todos.filter(todo => todo.done)
  }
}

const actions = {
  async getTodos ({ dispatch, commit }) {
    const todos = await Api.allTodos()
    commit('setTodos', todos)
  }
}

export const mutations = {
  addTodo (state, todo) {
    state.todos.push(todo)
  },
  setTodoDone (state, todoId) {
    const arr = state.todos.filter(todo => todo.id === todoId)
    if (arr.length > 0) {
      arr[0].done = true
    } else {
      console.error("Can't find todo item with id: " + todoId)
    }
  },
  setTodos (state, todos) {
    state.todos = todos
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
