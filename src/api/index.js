import Todo from '../model/todo'

let todo1 = Todo.with({
  id: '1',
  title: 'Title 1',
  description: 'Description 1',
  done: true
})
let todos = [todo1]

export default class Api {
  static async allTodos () {
    return Promise.resolve(todos)
  }
}
